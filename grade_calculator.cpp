#include "grade_calculator.h"
#include "ui_grade_calculator.h"

grade_calculator::grade_calculator(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::grade_calculator)
{
    ui->setupUi(this);

    /* ****************************** *
     * Slider and Spinbox connections *
     * ****************************** */
    QObject::connect(ui->hw1s, SIGNAL(valueChanged(int)), ui->hw1sb, SLOT(setValue(int)));
    QObject::connect(ui->hw1sb, SIGNAL(valueChanged(int)), ui->hw1s, SLOT(setValue(int)));

    QObject::connect(ui->hw2s, SIGNAL(valueChanged(int)), ui->hw2sb, SLOT(setValue(int)));
    QObject::connect(ui->hw2sb, SIGNAL(valueChanged(int)), ui->hw2s, SLOT(setValue(int)));

    QObject::connect(ui->hw3s, SIGNAL(valueChanged(int)), ui->hw3sb, SLOT(setValue(int)));
    QObject::connect(ui->hw3sb, SIGNAL(valueChanged(int)), ui->hw3s, SLOT(setValue(int)));

    QObject::connect(ui->hw4s, SIGNAL(valueChanged(int)), ui->hw4sb, SLOT(setValue(int)));
    QObject::connect(ui->hw4sb, SIGNAL(valueChanged(int)), ui->hw4s, SLOT(setValue(int)));

    QObject::connect(ui->hw5s, SIGNAL(valueChanged(int)), ui->hw5sb, SLOT(setValue(int)));
    QObject::connect(ui->hw5sb, SIGNAL(valueChanged(int)), ui->hw5s, SLOT(setValue(int)));

    QObject::connect(ui->hw6s, SIGNAL(valueChanged(int)), ui->hw6sb, SLOT(setValue(int)));
    QObject::connect(ui->hw6sb, SIGNAL(valueChanged(int)), ui->hw6s, SLOT(setValue(int)));

    QObject::connect(ui->hw7s, SIGNAL(valueChanged(int)), ui->hw7sb, SLOT(setValue(int)));
    QObject::connect(ui->hw7sb, SIGNAL(valueChanged(int)), ui->hw7s, SLOT(setValue(int)));

    QObject::connect(ui->hw8s, SIGNAL(valueChanged(int)), ui->hw8sb, SLOT(setValue(int)));
    QObject::connect(ui->hw8sb, SIGNAL(valueChanged(int)), ui->hw8s, SLOT(setValue(int)));

    QObject::connect(ui->aux1s, SIGNAL(valueChanged(int)), ui->aux1sb, SLOT(setValue(int)));
    QObject::connect(ui->aux1sb, SIGNAL(valueChanged(int)), ui->aux1s, SLOT(setValue(int)));

    QObject::connect(ui->aux2s, SIGNAL(valueChanged(int)), ui->aux2sb, SLOT(setValue(int)));
    QObject::connect(ui->aux2sb, SIGNAL(valueChanged(int)), ui->aux2s, SLOT(setValue(int)));

    QObject::connect(ui->fs, SIGNAL(valueChanged(int)), ui->fsb, SLOT(setValue(int)));
    QObject::connect(ui->fsb, SIGNAL(valueChanged(int)), ui->fs, SLOT(setValue(int)));

    /* ******************************** *
     * Connections to grade calculation *
     * ******************************** */
    QObject::connect(ui->hw1sb, SIGNAL(valueChanged(int)), this, SLOT(calculate()));
    QObject::connect(ui->hw2sb, SIGNAL(valueChanged(int)), this, SLOT(calculate()));
    QObject::connect(ui->hw3sb, SIGNAL(valueChanged(int)), this, SLOT(calculate()));
    QObject::connect(ui->hw4sb, SIGNAL(valueChanged(int)), this, SLOT(calculate()));
    QObject::connect(ui->hw5sb, SIGNAL(valueChanged(int)), this, SLOT(calculate()));
    QObject::connect(ui->hw6sb, SIGNAL(valueChanged(int)), this, SLOT(calculate()));
    QObject::connect(ui->hw7sb, SIGNAL(valueChanged(int)), this, SLOT(calculate()));
    QObject::connect(ui->hw8sb, SIGNAL(valueChanged(int)), this, SLOT(calculate()));

    QObject::connect(ui->aux1sb, SIGNAL(valueChanged(int)), this, SLOT(calculate()));
    QObject::connect(ui->aux2sb, SIGNAL(valueChanged(int)), this, SLOT(calculate()));

    QObject::connect(ui->fsb, SIGNAL(valueChanged(int)), this, SLOT(calculate()));

    /* **************************** *
     * Connections to schema change *
     * **************************** */
    QObject::connect(ui->schema_a, SIGNAL(clicked()), this, SLOT(calculate()));
    QObject::connect(ui->schema_b, SIGNAL(clicked()), this, SLOT(calculate()));

    /* ******************************* *
     * Connections to Combo Box change *
     * ******************************* */
    ui->course_select->addItem("PIC10B - Intermediate Programming");
    ui->course_select->addItem("PIC10C - Advanced Programming");
    QObject::connect(ui->course_select, SIGNAL(currentIndexChanged(int)), this, SLOT(calculate()));
}

grade_calculator::~grade_calculator()
{
    delete ui;
}

void grade_calculator::calculate()
{
    // Checks which class it is
    if (ui->course_select->currentIndex() == PIC10B)
    {
        // Set labels
        ui->hw4->setText("HW 4");
        ui->hw5->setText("HW 5");
        ui->hw6->setText("HW 6");
        ui->hw7->setText("HW 7");
        ui->hw8->setText("HW 8");

        ui->aux2->setText("Midterm 2");
        schema_pic10b();
    }
    else
    {
        ui->hw4->setText("N/A");
        ui->hw5->setText("N/A");
        ui->hw6->setText("N/A");
        ui->hw7->setText("N/A");
        ui->hw8->setText("N/A");

        ui->aux2->setText("Final Project");
        schema_pic10c();
    }

    // Change the text on the calculator
    ui->overall_score_num->setText(QString::number(total));
}

void grade_calculator::schema_pic10b()
{
    // Set up HW score
    double hw_total = (
                        ui->hw1sb->value() + ui->hw2sb->value()
                      + ui->hw3sb->value() + ui->hw4sb->value()
                      + ui->hw5sb->value() + ui->hw6sb->value()
                      + ui->hw7sb->value() + ui->hw8sb->value()
                      ) / (double) HWB;

    // Set up total score
    total = hw_total * 0.25;

    // Check which schema to use
    if (ui->schema_a->isChecked())
    {
        total += (double) ui->aux1sb->value() * 0.2
               + (double) ui->aux2sb->value() * 0.2;
        total += (double) ui->fsb->value() * 0.35;
    }

    else
    {
        if (ui->aux2sb->value() < ui->aux1sb->value())
            total += (double) ui->aux1sb->value() * 0.3;
        else
            total += (double) ui->aux2sb->value() * 0.3;
        total += (double) ui->fsb->value() * 0.44;
    }

    return;
}

void grade_calculator::schema_pic10c()
{
    // Set up HW scores
    double hw_total = (ui->hw1sb->value() + ui->hw2sb->value() + ui->hw3sb->value()) / (double) HWC;

    //Set up total scores
    total = hw_total * 0.15;
    total += (double) ui->aux2sb->value() * 0.35;

    // Check which schema to use
    if (ui->schema_a->isChecked())
    {
        total += (double) ui->aux1sb->value() * 0.25;
        total += (double) ui->fsb->value() * 0.3;
    }

    else
    {
        total += (double) ui->fsb->value() * 0.5;
    }

    return;
}
