#ifndef GRADE_CALCULATOR_H
#define GRADE_CALCULATOR_H

#include <QMainWindow>

enum Course{PIC10B, PIC10C};
const int HWB = 8;
const int HWC = 3;

namespace Ui {
class grade_calculator;
}

class grade_calculator : public QMainWindow
{
    Q_OBJECT

public:
    explicit grade_calculator(QWidget *parent = 0);
    ~grade_calculator();

private slots:
    void calculate();

private:
    void schema_pic10b();
    void schema_pic10c();

    Ui::grade_calculator *ui;
    double total;
};

#endif // GRADE_CALCULATOR_H
