# Grade Calculator
This program serves as a basic grade calculator for two courses: PIC 10B and PIC 10C.

## Overview
This README is organized as follows:

1. Describe how the program works.
2. Describe some limitations to the current implementation

## How it works
The calculator is composed of three sections:

1. Homeworks
2. Midterms / Projects
3. Final

With this information, the program works as follows:

1. Select the course that you want to calculate the grade for.
2. Select which grading scheme you would like to calculate the grade for.
3. Input the grades into the corresponding sections.

Some notes:

1. To input a grade, either move the slider or type the number into the spinbox. Both will be updated regardless of which one you used to input the grade. 
2. The scores will automatically be updated every time you add / change a score, switch schemas, or switch between the courses.
3. Any sliders / spinboxes labeled as 'N/A' will not affect the score output.
 
## Limitations
There are several limitations to the current iteration of the program. They are listed below:

1. Users cannot input initial scores as doubles; only ints.
2. Users cannot add their own courses / grading schemes.
3. A minor detail, but the menu bar does nothing.

Hopefully, as time goes on, these will be resolved.
